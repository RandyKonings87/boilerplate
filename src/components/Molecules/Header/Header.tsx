import React from 'react';

export interface HeaderProps {
  title?: string;
}

export function Header({ title = 'Title' }: HeaderProps) {
  return (
    <header className="font-extrabold">
      <span>{title}</span>
    </header>
  );
}
