import merge from 'webpack-merge';
import { Configuration, HotModuleReplacementPlugin } from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import common from './webpack.common.config';

const config: Configuration = merge(common, {
  mode: 'development',
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
    new HotModuleReplacementPlugin()
  ],
  devtool: 'inline-source-map'
});

export default config;
