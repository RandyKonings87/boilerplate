import { Configuration } from 'webpack';
import merge from 'webpack-merge';
import webpackConfig from '../webpack.dev.config';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

const { resolve } = webpackConfig;

export default {
  stories: ['../stories'],
  addons: ["@storybook/addon-links", "@storybook/addon-essentials", '@chakra-ui/storybook-addon'],
  framework: "@storybook/react",
  core: {
    "builder": "webpack5"
  },
  webpackFinal: async (config: Configuration) => {
    return merge(config,
      {
        resolve, 
        module: {
          rules: [{
            test: /\.scss$/,
            use: [MiniCssExtractPlugin.loader,'css-loader', 'postcss-loader']
          }],  
        }, plugins: [
          new MiniCssExtractPlugin(),
        ]
      }
    );
  }
};
export const core = {
  builder: "webpack5"
};