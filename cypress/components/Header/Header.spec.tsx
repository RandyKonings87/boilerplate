import React from 'react';
import { composeStories } from '@storybook/testing-react';
import { mount } from '@cypress/react';
import * as stories from '@stories/components/atoms/Header.stories';

const { Primary } = composeStories(stories);
describe('Primary', () => {
  it('should render header component', () => {
    mount(<Primary />);
    cy.findByText('Title').should('exist');
  });
});
