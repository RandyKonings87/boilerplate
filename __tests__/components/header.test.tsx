import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import React from 'react';
import { Header } from '@components/Molecules/Header/Header';

it('renders header', () => {
  render(<Header />);
  expect(screen.findByText('Header')).toBeTruthy();
});
