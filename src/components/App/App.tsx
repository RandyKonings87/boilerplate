import { Header } from '@components/Molecules/Header/Header';
import React from 'react';

function App() {
  return (
    <div>
      <Header />
    </div>
  );
}

export default App;
